<?php

require_once dirname(__DIR__) . '/tests/DetailedException.php';

use Helpers\ApiTestCase;

class SanityTest extends ApiTestCase
{
    const TestErrorDetails = [
        ['foo' => 'foo', 'bar' => 'bar'],
    ];

    /**
     * @expectedException DetailedException
     * @expectedExceptionErrorDetails SanityTest::TestErrorDetails
     */
    public function testCannotBeCreatedFromInvalidEmailAddress()
    {
        $e = new DetailedException('test exc');
        $e->setErrorDetails([
            ['foo' => 'foo', 'bar' => 'bar'],
            // ['foo' => 'foo', 'bar' => 'baz'],
        ]);
        throw $e;
    }

    public function testCanBeUsedAsString()
    {
        $this->assertEquals(
            'user@example.com',
            'user@example.com'
        );
    }
}
