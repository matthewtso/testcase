<?php

class DetailedException extends Exception {
    protected $errorDetails;

    public function getErrorDetails() {
        return $this->errorDetails;
    }

    public function setErrorDetails($errorDetails) {
        $this->errorDetails = $errorDetails;
    }
}
