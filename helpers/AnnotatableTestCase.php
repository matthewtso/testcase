<?php

namespace Helpers;

use PHPUnit\Framework\TestCase;
use PHPUnit_Util_Test;

/**
 * A test case that makes the @expectedExceptionErrorDetails annotation
 * available to the test's comment docs.
 */
class AnnotatableTestCase extends TestCase
{
    /** @var string */
    protected $name;
    
    protected $data;
    
    // Shadow parent dependencyInputs
    protected $dependencyInput = [];
    
    /** @var \Throwable */
    protected $expectedException;
    
    /** @var int */
    protected $expectedExceptionCode;
    
    /** @var string */
    protected $expectedExceptionMessage;
    
    protected $expectedExceptionMessageRegExp;
    
    /** @var array */
    protected $expectedExpectedExceptionErrorDetails;
    
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $this->data     = $data;
        parent::__construct($name, $data, $dataName);
    }

    public function setName($name) {
        $this->name = $name;
        parent::setName($name);
    }

    public function getExpectedExceptionErrorDetails() {
        return $this->expectedExpectedExceptionErrorDetails;
    }

    public function setExpectedExceptionErrorDetails($codeString) {
        $this->expectedExpectedExceptionErrorDetails = $codeString;
    }

    public function runBare() {
        $this->setExpectedExceptionErrorDetailsFromAnnotation();
        parent::runBare();
    }

    protected function setExpectedExceptionErrorDetailsFromAnnotation() {
        try {
            $annotations = $this->getAnnotations();

            if (isset($annotations['method']['expectedExceptionErrorDetails'])) {
                $this->setExpectedExceptionErrorDetails(
                    self::parseAnnotationContent(
                        $annotations['method']['expectedExceptionErrorDetails'][0]
                    )
                );
            }
        } catch (ReflectionException $e) {
        }
    }

    protected static function parseAnnotationContent($message)
    {
        if (strpos($message, '::') !== false && count(explode('::', $message)) == 2) {
            if (defined($message)) {
                $message = constant($message);
            }
        }

        return $message;
    }

    public function setDependencyInput(array $dependencyInput)
    {
        parent::setDependencyInput($dependencyInput);
        $this->dependencyInput = $dependencyInput;
    }

    /**
     * @param string $exception
     */
    public function expectException($exception)
    {
        parent::expectException($exception);
        $this->expectedException = $exception;
    }

    /**
     * @param int|string $code
     *
     * @throws PHPUnit_Framework_Exception
     */
    public function expectExceptionCode($code)
    {
        parent::expectExceptionCode();
        $this->expectedExceptionCode = $code;
    }

    /**
     * @param string $message
     *
     * @throws PHPUnit_Framework_Exception
     */
    public function expectExceptionMessage($message)
    {
        parent::expectExceptionMessage($message);
        $this->expectedExceptionMessage = $message;
    }

    /**
     * @param string $messageRegExp
     *
     * @throws PHPUnit_Framework_Exception
     */
    public function expectExceptionMessageRegExp($messageRegExp)
    {
        parent::expectExceptionMessageRegExp($messageRegExp);
        $this->expectedExceptionMessageRegExp = $messageRegExp;
    }

    /**
     * Override to run the test and assert its state.
     *
     * @return mixed
     *
     * @throws Exception|PHPUnit_Framework_Exception
     * @throws PHPUnit_Framework_Exception
     */
    protected function runTest()
    {
        // FIXME: Completely override parent's runTest (so that the test method is only called once!!!)
        // But design this so that it can be modularly extended!!
        // $parentTestResult = parent::runTest();

        try {
            $class  = new \ReflectionClass($this);
            $method = $class->getMethod($this->name);
        } catch (ReflectionException $e) {
            $this->fail($e->getMessage());
        }

        $testArguments = array_merge($this->data, $this->dependencyInput);

        $registerMocks = new \ReflectionMethod('PHPUnit_Framework_TestCase', 'registerMockObjectsFromTestArguments');
        $registerMocks->setAccessible(true);
        $registerMocks->invoke($this, $testArguments);

        try {
            $testResult = $method->invokeArgs($this, $testArguments);
        } catch (\Throwable $_e) {
            $e = $_e;
        } catch (\Exception $_e) {
            $e = $_e;
        }

        if (isset($e)) {
            $checkException = false;

            if (!($e instanceof PHPUnit_Framework_SkippedTestError) && is_string($this->expectedException)) {
                $checkException = true;

                if ($e instanceof PHPUnit_Framework_Exception) {
                    $checkException = false;
                }

                $reflector = new \ReflectionClass($this->expectedException);

                if ($this->expectedException === 'PHPUnit_Framework_Exception' ||
                    $this->expectedException === '\PHPUnit_Framework_Exception' ||
                    $reflector->isSubclassOf('PHPUnit_Framework_Exception')) {
                    $checkException = true;
                }
            }


            if ($checkException) {
                $this->checkException($e);
                return;
            } else {
                throw $e;
            }
        }

        $this->checkPass();

        return $testResult;
    }

    protected function checkException($e) {
        $this->assertThat(
            $e,
            new \PHPUnit_Framework_Constraint_Exception(
                $this->expectedException
            )
        );

        if ($this->expectedExceptionMessage !== null) {
            $this->assertThat(
                $e,
                new \PHPUnit_Framework_Constraint_ExceptionMessage(
                    $this->expectedExceptionMessage
                )
            );
        }

        if ($this->expectedExceptionMessageRegExp !== null) {
            $this->assertThat(
                $e,
                new \PHPUnit_Framework_Constraint_ExceptionMessageRegExp(
                    $this->expectedExceptionMessageRegExp
                )
            );
        }

        if ($this->expectedExceptionCode !== null) {
            $this->assertThat(
                $e,
                new \PHPUnit_Framework_Constraint_ExceptionCode(
                    $this->expectedExceptionCode
                )
            );
        }

        // // Extra
        // if ($this->expectedExpectedExceptionErrorDetails !== null) {
        //     $this->assertThat(
        //         '1234', //$e,
        //         new \PHPUnit_Framework_Constraint_IsEqual(
        //             $this->expectedExpectedExceptionErrorDetails
        //         )
        //     );
        // }
    }

    protected function checkPass() {
        if ($this->expectedException !== null) {
            $this->assertThat(
                null,
                new \PHPUnit_Framework_Constraint_Exception(
                    $this->expectedException
                )
            );
        }
    }

    protected function setExceptionAnnotations() {
        // $exceptionAnnotationIdentifiers = $this->getExceptionAnnotationIdentifiers();
        // $expectedExceptionProperties = [];
        
        // $annotations = $this->getAnnotations();

        try {
            // $foreach ($exceptionAnnotations as $annotation) {
            //     if (isset($annotations['method'][$annotations])) {
            //         $expectedExceptionProperties[$annotation] = self::parseAnnotationContent(
            //             $annotations['method'][$annotations][0]
            //         );
            //     }
            // }

            $annotations = $this->getAnnotations();

            if (isset($annotations['method']['expectedExceptionErrorDetails'])) {
                $this->setExpectedExceptionErrorDetails(
                    self::parseAnnotationContent(
                        $annotations['method']['expectedExceptionErrorDetails'][0]
                    )
                );
            }
        } catch (ReflectionException $e) {
        }
    }
}
