<?php

namespace Helpers;

class ApiTestCase extends AnnotatableTestCase {
    // protected function getExceptionAnnotations() {
    //     return [
    //         'expectedExceptionErrorDetails' => \PHPUnit_Framework_Constraint_Exception,
    //     ];
    // }

    protected function checkException($e) {
        parent::checkException($e);

        // Extra
        if ($this->expectedExpectedExceptionErrorDetails !== null) {
            $this->assertThat(
                $e,
                new Constraints\Constraint_ExceptionErrorDetails(
                    $this->expectedExpectedExceptionErrorDetails
                )
            );
        }
    }
}
