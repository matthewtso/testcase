<?php

namespace Helpers\Constraints;

class Constraint_ExceptionErrorDetails extends \PHPUnit_Framework_Constraint
{
    /** @var array */
    protected $expectedErrorDetails;

    /**
     * @param string $className
     */
    public function __construct($errorDetails) {
        parent::__construct();
        $this->expectedErrorDetails = $errorDetails;
    }

    /**
     * Evaluates the constraint for parameter $other. Returns true if the
     * constraint is met, false otherwise.
     *
     * @param mixed $other Value or object to evaluate.
     *
     * @return bool
     */
    protected function matches($other) {
        $diff = $this->diff($other->getErrorDetails(), $this->expectedErrorDetails);
        return count($diff) === 0;
    }

    /**
     * Returns the description of the failure
     *
     * The beginning of failure messages is "Failed asserting that" in most
     * cases. This method should return the second part of that sentence.
     *
     * @param mixed $other Evaluated value or object.
     *
     * @return string
     */
    protected function failureDescription($other) {
        return sprintf(
            "the exception's error details are equal:\n%s",
            $this->formatDiff(
                $this->expectedErrorDetails,
                $other->getErrorDetails()));
    }

    protected function intersect($a, $b) {
        $intersection = [];
        foreach ($a as $el) {
            if (array_search($el, $b) !== false) {
                $intersection[] = $el;
            }
        }
        return $intersection;
    }

    protected function diff($a, $b) {
        $diff = $a;
        foreach ($a as $el) {
            $foundIndex = array_search($el, $b);
            if ($foundIndex !== false) {
                unset($diff[$foundIndex]);
            }
        }
        return $diff;
    }

    protected function formatDiff($expected, $actual) {
        $union = $this->formatErrorDetails('  ', $this->intersect($expected, $actual));
        $omissions = $this->formatErrorDetails('- ', $this->diff($expected, $actual));
        $additions = $this->formatErrorDetails('+ ', $this->diff($actual, $expected));
        $all = array_merge($union, $omissions, $additions);

        return sprintf("[\n%s\n]", join(",\n", $all));
    }

    protected function formatErrorDetails($append, $errorDetails) {
        $details = array_map(['self', 'formatErrorDetail'], $errorDetails);
        return array_map(function($el) use ($append) {
            $appended = array_map($this->makeAppender($append), explode("\n", $el));
            return join("\n", $appended);
        }, $details);
    }

    protected function formatErrorDetail($errorDetail) {
        return sprintf("%s", json_encode($errorDetail, JSON_PRETTY_PRINT));
    }

    public static function makeAppender($append) {
        return function ($content) use ($append) {
            return $append . $content;
        };
    }

    /**
     * Returns a string representation of the constraint.
     *
     * @return string
     */
    public function toString() {
        return 'exception error details are ';
    }
}
