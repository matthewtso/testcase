## Install

```
composer install
```

## Running

```
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/SanityTest.php
```
